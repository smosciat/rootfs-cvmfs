# rootfs-cvmfs

The RFC for hosting the rootfs directory on CVMFS

**ABORTED** This solution as a big design flaw: Avoid to move the whole image

# Goals

A simple way to use *standard* OCI (docker) images with CVMFS

# Why now?

Docker doesn't provide a way to index layers by hash, so it is extremely complex to host the layers into CVMFS and have docker use them.
We went around this limitation with the concept of thin images and the work done by Nikola.
Lately however, Docker will not be used anymore as default in K8S and docker itself is moving its runtime component into containerd.
containerd opens new opportunities since it use a root directory (`rootfs`) where the layers are stored in a way friendly with CVMFS

# Proposed solution

Since `containerd` stores all its state inside the `rootfs` directory, why we don't serve that directory directly via CVMFS?

# Issues and solution

## Writable layer

The `rootfs` dir must be writable by `containerd` so it cannot be served directly from CVMFS.

A possible solution to this is to use overlayfs to provide a writable directory on top of the `/cvmfs/container.cern.ch/rootfs` directory and have the containerd deamon running in the host use such directory as `rootfs`

```
# use overlay to provide the writable layer
mount -t overlay overlay -o lowerdir=/cvmfs/container.cern.ch/rootfs,workdir=/foo/work,upperdir=/foo/upper /containerd/rootfs

# use the above layer with containerd
containerd --rootfs=/containerd/rootfs
```

## Overlay on top of overlay

`containerd` as default use overlayfs to mount the layers one on top of the other and provide to the user the workspace.
However is not possible to have an overlay layer stacked on top of the other.
So if we have already used overlay to provide the writable layer for the `rootfs` directory we need to find another way.

The solution is to instruct `containerd` to don't use overlayfs but a different snapshotter.
There are several possibilities here, but the `native` snapshotter seems the simplest and more importantly seems to work.

## Instruct containerd to don't use the default snapshotter

How do we instruct containerd to don't use the overlayfs snapshotter but the native one if my biggest concern, I guess that there are flags to set in k8s and docker. However I have never tested those flags.

## Create the rootfs on CVMFS

There is no magic in the `rootfs` dir, the simplest thing to do is to just have a containerd deamon pull all the images we are interested in into a folder and then let CVMFS ingest such folder. A simple copy and paste should be enough.

## Changes in the underlying /cvmfs directory

The host should have a time-consistent view of the directory hosted in `/cvmfs`. 
(We should not change the content of that directory even if a new CVMFS manifest is available)

## Avoid to move the whole image

The whole use of CVMFS is to avoid materializing all the files until they are needed.
Unfortunately the native snapshotter works by copying, bit by bit, the files from a directory into another, this will render pointless the use of CVMFS.

:(

EOF